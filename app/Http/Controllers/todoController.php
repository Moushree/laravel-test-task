<?php

namespace App\Http\Controllers;

use App\Models\todoModel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Asana\Client;
class todoController extends Controller
{
    public function index()
    {
        return view('todo');
    }
    public function insert(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'task' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'status'=>400,
                'errors'=>$validator->errors()
            ]);
        }
        else{
            $task = $r->task;
            $obj = new todoModel();
            $obj->task = $task;
            $obj->save();

            return response()->json([
                'status' => 200,
                'message' => 'Task added successfully'
            ]);


        }
    }
    public function show()
    {
        $task_list = todoModel::all();
        return response()->json([
            'task_list' => $task_list
        ]);
    }
    public function delete(Request $r)
    {
        $id=$r->id;
        $obj=todoModel::find($id);
        $obj->delete();
        $task_list = todoModel::all();
        return response()->json([
            'task_list' => $task_list
        ]);
    }
    public function edit(Request $r)
    {
        $id=$r->id;
        $task_edit=todoModel::find($id);
        if($task_edit)
        {
            return response()->json([
                'status' => 200,
                'task_edit' => $task_edit
            ]);
        }
        else{
            return response()->json([
                'status' => 404,
                'msg' => 'task not found'
            ]);
        }
       
    }
    public function edit_insert(Request $r)
    {
        $validator = Validator::make($r->all(), [
            'task' => 'required'
        ]);

        if($validator->fails())
        {
            return response()->json([
                'status'=>404,
                'errors'=>$validator->errors()
            ]);
        }
        else{
            $id = $r->id;
            $task_obj=todoModel::find($id);
            if($task_obj)
            {
                $task_obj->task = $r->task;
                $task_obj->update();

                return response()->json([
                    'status' => 200,
                    'message' => 'Task updated successfully'
                ]);
            }
            else
            {
                return response()->json([
                    'status' => 404,
                    'message' => 'Task not updated '
                ]);
            }
            
            


        }
    }
    public function asana_fun()
    {
      $asana_token = Client::accessToken('1/1200730339748897:543c7a4aa0ceb5f9a2970f7ec480fd71');

      $asana_project = $asana_token->get('/projects',[]);
      print_r($asana_project);
      $task = $asana_token->get('/projects/'.$asana_project[0]->gid.'/tasks',[]);
      print_r($task);
        
    }
}
