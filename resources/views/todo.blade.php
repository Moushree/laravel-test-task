<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"/>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">

  <h2>Todo List</h2>
  @if(Session::has('msg'))
  <div class="alert alert-success alert-dismissible fade show">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <strong>{{Session::get('msg')}}</strong>
  </div>
  @endif
  <form>
      @csrf
    <div class="form-group">
      <label for="task">Add Task:</label>
     
      <input type="text" class="form-control" id="task" placeholder="Enter task" name="task">
      <span id="task_error"></span>
    </div>

    <button type="submit" class="btn btn-primary" id="todo-btn">Submit</button>
  </form>
</div>
<div class="container pt-5">
                  
  <h3 class="text-center pb-2">Todo List</h3>  
  <span id="edit_error"></span>    
  <span id="edit_success"></span>          
  <table class="table table-striped">
    <thead>
      <tr>
        <th>Tasks</th>
        <th>Update</th>
        <th>Remove</th>
      </tr>
    </thead>
    <tbody>
    </tbody>
  </table>
<!-- The Modal -->
<div class="modal" id="editModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Update Todo List</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <input type="hidden" class="form-control" id="edit_id" name="edit_id">

        <input type="text" class="form-control" id="edit_task" placeholder="Enter task" name="edit_task">
        <span id="edit_task_error"></span>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary" id="edit-todo-btn">Update</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>

    </div>
  </div>
</div>
  
</div>
<script>
      function del(id){
                      
                      if(confirm('Are you sure?')){
                        $.ajax({
                          type:'GET',
                          url:'http://127.0.0.1:8000/delete/'+id,
                          dataType:'json',
                          success:function(response)
                          {
                            
                            $('tbody').html("");
                            $.each(response.task_list, function (key, items){
                              
                              $('tbody').append(
      
                                '<tr>\
                                  <td>'+items.task+'</td>\
                                  <td><a href="javascript:void(0);" onclick="del('+items.id+')" class="btn btn-success"><i class="fa fa-pencil-square-o"></i></a></td>\
                                  <td><a href="javascript:void(0);" onclick="del('+items.id+')" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>\
                                </tr>');
                            })
                          }
                        });
                      }
      }
      function edit(id){

        $('#editModal').modal('show');
                      
                     
                        $.ajax({
                          type:'GET',
                          url:'http://127.0.0.1:8000/edit/'+id,
                          dataType:'json',
                          success:function(response)
                          {
                            if(response.status == 404)
                            {
                                $('#edit_error').html("");
                                $('#edit_error').css('color','red')
                                $('#edit_error').text(response.msg)
                                
                            }
                            else{
                              $('#edit_id').val(response.task_edit.id);
                              $('#edit_task').val(response.task_edit.task);
                              
                            }
                          }
                          
                        });
                     
      }

    $(document).ready(function(){
      
        
      fetchtask();

      function fetchtask()
      {
        $.ajax({
          type:'GET',
          url:'http://127.0.0.1:8000/show',
          dataType:'json',
          success:function(response)
          {
            
            $('tbody').html("");
            $.each(response.task_list, function (key, items){
            
              $('tbody').append(

                '<tr>\
                  <td>'+items.task+'</td>\
                  <td><a href="javascript:void(0);" onclick="edit('+items.id+')" class="btn btn-success"><i class="fa fa-pencil-square-o"></i></a></td>\
                  <td><a href="javascript:void(0);" onclick="del('+items.id+')" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>\
                </tr>');
            })
          }
        });

      }

        $(document).on('click','#todo-btn',function(e){

            e.preventDefault();
                
                var data={
                    'task' : $('#task').val()
                }
               console.log(data);
                $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                      }
                  });
                $.ajax({

                    type:'POST',
                    url:'http://127.0.0.1:8000/insert',
                    data:data,
                    dataType:'json',
                    success:function(response){
                        // console.log(response);
                        if(response.status == 400)
                      {
                          $('#task_error').html("");
                          $('#task_error').css('color','red')
                          $('#task_error').text('**'+response.errors.task)
                          
                      }
                      else{
                        $('#barcode_erroe').html("");
                          $('#task_error').css('color','red')
                          $('#task_error').text("")
                        $('#task').val("")
                        fetchtask();
                      }
                    }
                    

                })
            

        });
        $(document).on('click','#edit-todo-btn',function(e){

          e.preventDefault();
              
              var data={
                  'id' : $('#edit_id').val(),
                  'task' : $('#edit_task').val()
              }
            console.log(data);
              $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
              $.ajax({

                  type:'POST',
                  url:'http://127.0.0.1:8000/edit-insert',
                  data:data,
                  dataType:'json',
                  success:function(response){
                      // console.log(response);
                      if(response.status == 400)
                      {
                          $('#edit_task_error').html("");
                          $('#edit_task_error').css('color','red')
                          $('#edit_task_error').text('**'+response.errors.task)
                          
                      }
                      else if(response.status == 404)
                      {
                        $('#edit_task_error').html("");
                        $('#edit_task_error').css('color','red')
                        $('#edit_task_error').text(response.message)
                      }
                      else
                      {
                        $('#edit_task_error').html("")
                        $('#edit_success').css('color','green')
                        $('#edit_success').text(response.message)
                        $('#editModal').modal('hide');
                        fetchtask();
                      }
                   
                  }
                  

              })


          });

    });

</script>
</body>
</html>
