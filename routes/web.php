<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\todoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/todo',[todoController::class,'index']);
Route::post('/insert',[todoController::class,'insert']);
Route::get('/show',[todoController::class,'show']);
Route::get('/delete/{id}',[todoController::class,'delete']);
Route::get('/edit/{id}',[todoController::class,'edit']);
Route::post('/edit-insert',[todoController::class,'edit_insert']);
Route::get('/asana-list',[todoController::class,'asana_fun']);